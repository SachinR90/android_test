package com.test.model;

import com.google.gson.annotations.SerializedName;

public class Order {
    @SerializedName("order_id")
    public int orderId;
    @SerializedName("arrives_at_utc")
    public long arrivesAt;
    @SerializedName("paid_with")
    public String paidWith;
}
