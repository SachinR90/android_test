package com.test.repository.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.test.model.Order;

import java.util.List;

public class OrderResponse {
    @SerializedName("orders")
    @Expose
    public List<Order> orderList = null;

    public OrderResponse(List<Order> orderList){
        this.orderList = orderList;
    }
}
