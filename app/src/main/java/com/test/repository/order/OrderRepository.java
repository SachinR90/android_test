package com.test.repository.order;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.test.model.Order;
import com.test.network.ApiClient;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderRepository {
    private static final String TAG = OrderRepository.class.getSimpleName();
    private OrderRequest orderRequest;

    public OrderRepository() {
        orderRequest = ApiClient.getClient().create(OrderRequest.class);
    }

    public LiveData<OrderResponse> getOrders(){
        final MutableLiveData<OrderResponse> data = new MutableLiveData<>();
        orderRequest.getOrders().enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                Log.d(TAG, "onResponse response:: " + response);

                if (response.body() != null) {
                    data.setValue(response.body());
                }else{
                    data.setValue(new OrderResponse(new ArrayList<Order>()));
                }
            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                //set empty list on failure
                data.setValue(new OrderResponse(new ArrayList<Order>()));
            }
        });
        return data;
    }
}
