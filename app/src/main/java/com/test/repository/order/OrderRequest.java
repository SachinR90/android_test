package com.test.repository.order;

import retrofit2.Call;
import retrofit2.http.GET;

public interface OrderRequest {
    @GET("/test/orders")
    Call<OrderResponse> getOrders();
}
