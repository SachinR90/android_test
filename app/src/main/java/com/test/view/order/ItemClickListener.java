package com.test.view.order;

import com.test.model.Order;

public interface ItemClickListener {
    void onItemClick(int position, Order data);
}
