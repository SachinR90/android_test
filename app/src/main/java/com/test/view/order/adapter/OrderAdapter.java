package com.test.view.order.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.test.R;
import com.test.model.Order;
import com.test.view.order.ItemClickListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {

    private ItemClickListener listener;
    private Context context;
    private ArrayList<Order> orderList;
    //in mock up only time is give, but to see data in sorted order we need to show date as well.
    //hence using dd-MMM-yyy as readable format
    private SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy HH:mm a", Locale.getDefault());
    private View emptyView;
    public OrderAdapter(Context context, ArrayList<Order> orderList) {
        this.context = context;
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public OrderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_order, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderAdapter.ViewHolder viewHolder, final int position) {
        final Order order = orderList.get(position);
        viewHolder.orderNumber.setText("#" + order.orderId);
        viewHolder.arrivesAt.setText(format.format(new Date(order.arrivesAt)));
        viewHolder.paidWith.setText(order.paidWith);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(position, orderList.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView orderNumber;
        private final TextView arrivesAt;
        private final TextView paidWith;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            orderNumber = itemView.findViewById(R.id.tv_order_id);
            arrivesAt = itemView.findViewById(R.id.tv_order_arrives_at_value);
            paidWith = itemView.findViewById(R.id.tv_order_paid_with_value);
        }
    }

    public void setItems(List<Order> orderList) {
        this.orderList = new ArrayList<>();
        this.orderList.addAll(orderList);
        notifyDataSetChanged();
        if (this.orderList.isEmpty()){
            this.emptyView.setVisibility(View.VISIBLE);
        }else{
            this.emptyView.setVisibility(View.GONE);
        }
    }

    public void setClickListener(ItemClickListener listener){
        this.listener = listener;
    }

    public void setEmptyView(View view){
        this.emptyView = view;
    }

    public void hideEmptyView(){
        this.emptyView.setVisibility(View.GONE);
    }
}
