package com.test.view.order;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.test.R;
import com.test.model.Order;
import com.test.repository.order.OrderResponse;
import com.test.view.order.adapter.OrderAdapter;
import com.test.view.order_details.OrderDetailsActivity;
import com.test.view_model.OrderViewModel;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import me.relex.circleindicator.CircleIndicator2;

public class MainActivity extends AppCompatActivity implements ItemClickListener {
    public static final String TAG = MainActivity.class.getSimpleName();
    private OrderViewModel viewModel;
    private OrderAdapter adapter;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initialize();

        progressBar.setVisibility(View.VISIBLE);
        adapter.hideEmptyView();
        getOrderList();
    }

    private void initialize() {
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        View emptyView = findViewById(R.id.ll_empty_view);
        progressBar = findViewById(R.id.progress_circular);
        Button btn_try_again = findViewById(R.id.btn_try_again);
        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);

        adapter = new OrderAdapter(this, new ArrayList<Order>());
        recyclerView.setAdapter(adapter);
        adapter.setClickListener(this);
        adapter.setEmptyView(emptyView);

        CircleIndicator2 rv_page_indicator = findViewById(R.id.rv_page_indicator);
        rv_page_indicator.attachToRecyclerView(recyclerView,snapHelper);
        adapter.registerAdapterDataObserver(rv_page_indicator.getAdapterDataObserver());
        //init click listener on button
        btn_try_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                adapter.hideEmptyView();
                retryOrder();
            }
        });
        // View Model
        viewModel = ViewModelProviders.of(this).get(OrderViewModel.class);
    }

    /**
     * get movies articles from news api
     *
     * @param @null
     */
    private void getOrderList() {
        viewModel.getOrderResponseLiveData().observe(this, new OrderObserver());
    }

    private void retryOrder(){
        viewModel.retryOrder().observe(this,new OrderObserver());
    }

    @Override
    public void onItemClick(int position, Order data) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, OrderDetailsActivity.class);
                startActivity(intent);
            }
        });
    }

    private class OrderObserver implements Observer<OrderResponse> {

        @Override
        public void onChanged(OrderResponse orderResponse) {
            progressBar.setVisibility(View.GONE);
            if (orderResponse != null) {
                ArrayList<Order> sortedList = new ArrayList<>(orderResponse.orderList);
                Collections.sort(sortedList, new Comparator<Order>() {
                    @Override
                    public int compare(Order order1, Order order2) {
                        int result;
                        if (order1.arrivesAt == order2.arrivesAt){
                            result = 0;
                        }else if (order1.arrivesAt > order2.arrivesAt){
                            result = 1;
                        }else{
                            result = -1;
                        }
                        return result;
                    }
                });
                adapter.setItems(sortedList);
            }
        }
    }
}
