package com.test.view_model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.test.repository.order.OrderRepository;
import com.test.repository.order.OrderResponse;

public class OrderViewModel extends AndroidViewModel {
    private LiveData<OrderResponse> orderResponseLiveData;
    OrderRepository repository;
    public OrderViewModel(@NonNull Application application) {
        super(application);
        repository = new OrderRepository();
        this.orderResponseLiveData = repository.getOrders();
    }
    public LiveData<OrderResponse> getOrderResponseLiveData() {
        return orderResponseLiveData;
    }

    public LiveData<OrderResponse> retryOrder(){
        this.orderResponseLiveData = repository.getOrders();
        return getOrderResponseLiveData();
    }
}
