# android_test

Android Test
Using following Libraries

 - Live Data Extensions - androidx.lifecycle:lifecycle-extensions:2.1.0
 - Retrofit - com.squareup.retrofit2:retrofit:2.6.2
    - Retrofit converter - com.squareup.retrofit2:converter-gson:2.6.2
 - Android X
    - RecyclerView  - androidx.recyclerview:recyclerview:1.0.0'
    - CardView - androidx.cardview:cardview:1.0.0'
 - Circle indicator - me.relex:circleindicator:1.3.2
 - Tested on One Plus 3 android version 8.0.0
 - min SDK version 21
 - target SDK version 28